'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">fcskyui-library documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                        <li class="link">
                            <a href="dependencies.html" data-type="chapter-link">
                                <span class="icon ion-ios-list"></span>Dependencies
                            </a>
                        </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse" ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/FcskySharedLibraryModule.html" data-type="entity-link">FcskySharedLibraryModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-FcskySharedLibraryModule-e25776a307c81cba901d99b597f34c51"' : 'data-target="#xs-components-links-module-FcskySharedLibraryModule-e25776a307c81cba901d99b597f34c51"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FcskySharedLibraryModule-e25776a307c81cba901d99b597f34c51"' :
                                            'id="xs-components-links-module-FcskySharedLibraryModule-e25776a307c81cba901d99b597f34c51"' }>
                                            <li class="link">
                                                <a href="components/ActionsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ActionsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/BreadcrumbComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">BreadcrumbComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ButtonComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ButtonComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CheckboxComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CheckboxComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CheckboxGroupComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CheckboxGroupComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/DatepickerComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DatepickerComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/DropdownAmcComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DropdownAmcComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/DropdownComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DropdownComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FcskySharedLibraryComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">FcskySharedLibraryComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FormBuilderComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">FormBuilderComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FormFieldsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">FormFieldsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FormSectionComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">FormSectionComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FormTabComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">FormTabComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/InputComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">InputComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/LabelComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">LabelComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/LinkComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">LinkComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/OptionsListComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">OptionsListComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PageHeaderComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PageHeaderComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SearchBoxComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SearchBoxComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SearchComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SearchComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SectionComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SectionComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SectionTitleComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SectionTitleComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SelectGroupComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SelectGroupComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SelectOptionComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SelectOptionComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SlideToggleComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SlideToggleComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SliderComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SliderComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/StickyFooterComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">StickyFooterComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SvgComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SvgComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TableComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TableComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TextAreaComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TextAreaComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TooltipComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TooltipComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#directives-links-module-FcskySharedLibraryModule-e25776a307c81cba901d99b597f34c51"' : 'data-target="#xs-directives-links-module-FcskySharedLibraryModule-e25776a307c81cba901d99b597f34c51"' }>
                                        <span class="icon ion-md-code-working"></span>
                                        <span>Directives</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="directives-links-module-FcskySharedLibraryModule-e25776a307c81cba901d99b597f34c51"' :
                                        'id="xs-directives-links-module-FcskySharedLibraryModule-e25776a307c81cba901d99b597f34c51"' }>
                                        <li class="link">
                                            <a href="directives/TooltipDirective.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules">TooltipDirective</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-FcskySharedLibraryModule-e25776a307c81cba901d99b597f34c51"' : 'data-target="#xs-injectables-links-module-FcskySharedLibraryModule-e25776a307c81cba901d99b597f34c51"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-FcskySharedLibraryModule-e25776a307c81cba901d99b597f34c51"' :
                                        'id="xs-injectables-links-module-FcskySharedLibraryModule-e25776a307c81cba901d99b597f34c51"' }>
                                        <li class="link">
                                            <a href="injectables/FormBuilderDataService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>FormBuilderDataService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/GridTableVirtualScrollModule.html" data-type="entity-link">GridTableVirtualScrollModule</a>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#directives-links-module-GridTableVirtualScrollModule-6702a9e25870b20487823f341f511293"' : 'data-target="#xs-directives-links-module-GridTableVirtualScrollModule-6702a9e25870b20487823f341f511293"' }>
                                        <span class="icon ion-md-code-working"></span>
                                        <span>Directives</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="directives-links-module-GridTableVirtualScrollModule-6702a9e25870b20487823f341f511293"' :
                                        'id="xs-directives-links-module-GridTableVirtualScrollModule-6702a9e25870b20487823f341f511293"' }>
                                        <li class="link">
                                            <a href="directives/GridTableFixedVirtualScrollDirective.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules">GridTableFixedVirtualScrollDirective</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#components-links"' :
                            'data-target="#xs-components-links"' }>
                            <span class="icon ion-md-cog"></span>
                            <span>Components</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links"' : 'id="xs-components-links"' }>
                            <li class="link">
                                <a href="components/FormSelectTreeViewComponent.html" data-type="entity-link">FormSelectTreeViewComponent</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse" ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/CustomValidator.html" data-type="entity-link">CustomValidator</a>
                            </li>
                            <li class="link">
                                <a href="classes/GridTableDataSource.html" data-type="entity-link">GridTableDataSource</a>
                            </li>
                            <li class="link">
                                <a href="classes/GridTableVirtualScrollStrategy.html" data-type="entity-link">GridTableVirtualScrollStrategy</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/FcskySharedLibraryService.html" data-type="entity-link">FcskySharedLibraryService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse" ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/Action.html" data-type="entity-link">Action</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Actions.html" data-type="entity-link">Actions</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IBreadcrumb.html" data-type="entity-link">IBreadcrumb</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ICheckbox.html" data-type="entity-link">ICheckbox</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IClickedOption.html" data-type="entity-link">IClickedOption</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IColumn.html" data-type="entity-link">IColumn</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IControlValidation.html" data-type="entity-link">IControlValidation</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IFcForm.html" data-type="entity-link">IFcForm</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IField.html" data-type="entity-link">IField</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IFormButton.html" data-type="entity-link">IFormButton</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IFormSection.html" data-type="entity-link">IFormSection</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IFormTab.html" data-type="entity-link">IFormTab</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IGroupState.html" data-type="entity-link">IGroupState</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IItemNGroup.html" data-type="entity-link">IItemNGroup</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ISearchBoxValue.html" data-type="entity-link">ISearchBoxValue</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Item.html" data-type="entity-link">Item</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse" ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/enumerations.html" data-type="entity-link">Enums</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});