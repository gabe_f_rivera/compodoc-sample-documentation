# CompoDoc Sample Documentation

This is a sample of the documentation that is generated from Compodoc.

We would have to adjust some of our components, as we are not getting full documentation coverage it seems. However, using a tool like this could theoretically speed up development, as well as improve developer experience.

By automating the documentation, we no longer have to invest resources into creating it, the other benefit comes from the fact that developers no longer have to remember all the bits and pieces of their components to document, the machine does it.

The only downside I see, is that it serves more as just developer documentation, as there are no live use-case samples.

However there appears to be and integration being developed for compodoc and storybook, which would allow the best of both world.

Anyway, to see the documentation:

- clone the repo
- open the documentation/index.html file in a browser
- Boom! It lives
